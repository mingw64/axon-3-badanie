package com.example.demo;

import com.example.demo.Commands.CreateMessageCommand;
import com.example.demo.Commands.MarkReadMessageCommand;
import org.axonframework.commandhandling.*;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.commandhandling.gateway.DefaultCommandGateway;
import org.axonframework.eventhandling.AnnotationEventListenerAdapter;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine;
import org.axonframework.spring.config.EnableAxon;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.UUID;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@SpringBootApplication

@EnableAxon
public class DemoApplication {




	public static void main(String[] args) {


		//SpringApplication.run(DemoApplication.class, args);
        CommandBus commandBus = new SimpleCommandBus();
        CommandGateway commandGateway = new DefaultCommandGateway(commandBus);

        EventStore eventStore = new EmbeddedEventStore(new InMemoryEventStorageEngine());

        EventSourcingRepository<MessagesAggregate> repository =
                new EventSourcingRepository<MessagesAggregate>(MessagesAggregate.class, eventStore);

        AggregateAnnotationCommandHandler<MessagesAggregate> handler =
                new AggregateAnnotationCommandHandler<MessagesAggregate>(
                        MessagesAggregate.class, repository
                );
        handler.subscribe(commandBus);

        AnnotationEventListenerAdapter annotationEventListenerAdapter
                = new AnnotationEventListenerAdapter(new MessagesEventHandler());
        eventStore.subscribe(eventMessages -> eventMessages.forEach(e -> {
            try{
                annotationEventListenerAdapter.handle(e);
            }catch (Exception e1){
                throw new RuntimeException(e1);
            }
        }));

        String itemId = UUID.randomUUID().toString();
        commandGateway.send(new CreateMessageCommand(itemId, "Hello World"));
        commandGateway.send(new MarkReadMessageCommand(itemId));

	}
}
