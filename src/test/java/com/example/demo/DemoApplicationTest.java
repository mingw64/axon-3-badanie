package com.example.demo;

import com.example.demo.Commands.CreateMessageCommand;
import com.example.demo.Commands.MarkReadMessageCommand;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;


import java.util.UUID;

/*
      
 */


public class DemoApplicationTest {

    private FixtureConfiguration<MessagesAggregate> fixture;

    @Before
    public void setUp() throws Exception {
        fixture = new AggregateTestFixture<MessagesAggregate>(MessagesAggregate.class);

    }


    @Test
    public void giveAggregateRoot_whenCreateMessageCommand_thenShouldProduceMessageCreatedEvent() throws Exception {
        String eventText = "Wamma Say Hello World?";
        String id = UUID.randomUUID().toString();
        fixture.given()
                .when(new CreateMessageCommand(id, eventText))
                .expectEvents(new MessageCreatedEvent(id, eventText));
    }

    @Test
    public void test() throws  Exception{
        String id = UUID.randomUUID().toString();

        fixture.given(new MessageCreatedEvent(id, "Hello World"))
                .when(new MarkReadMessageCommand(id))
                .expectEvents(new MessageReadEvent(id));
    }

}